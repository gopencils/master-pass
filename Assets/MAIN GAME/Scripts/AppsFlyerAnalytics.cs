﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppsFlyerAnalytics : MonoBehaviour
{
    public static AppsFlyerAnalytics Instance;

    public string Dev_Key;
    public string App_ID;

    private void Awake()
    {
        Instance = (Instance == null) ? this : Instance;
    }

    private void Start()
    {
        /* Mandatory - set your AppsFlyer’s Developer key. */
        AppsFlyer.setAppsFlyerKey(Dev_Key);
        /* For detailed logging */
        /* AppsFlyer.setIsDebug (true); */
#if UNITY_IOS
        /* Mandatory - set your apple app ID
         NOTE: You should enter the number only and not the "ID" prefix */
        AppsFlyer.setAppID(App_ID);
        AppsFlyer.trackAppLaunch();
#elif UNITY_ANDROID
  /* Mandatory - set your Android package name */
  AppsFlyer.setAppID ("YOUR_ANDROID_PACKAGE_NAME_HERE");
  /* For getting the conversion data in Android, you need to add the "AppsFlyerTrackerCallbacks" listener.*/
  AppsFlyer.init ("YOUR_APPSFLYER_DEV_KEY","AppsFlyerTrackerCallbacks");
#endif
    }

    public void StartEvent()
    {
        AppsFlyer.trackRichEvent(AFInAppEvents.EVENT_START, new Dictionary<string, string>());
    }

    public void EndEvent()
    {
        AppsFlyer.trackRichEvent(AFInAppEvents.EVENT_END, new Dictionary<string, string>());
    }
}
